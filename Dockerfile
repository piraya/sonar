FROM sonarqube:6.3.1

# Download plugins into the Sonar plugin folder
ADD https://gitlab.com/piraya/sonar-rules/uploads/8f4d91d9400961a0ae13624747a158d2/sonar-piraya-rules-1.1.jar /opt/sonarqube/extensions/plugins
ADD https://gitlab.com/piraya/sonar-profile-plugin/uploads/d248339ebbb0b120942530db2b499534/sonar-profile-plugin-1.2.jar /opt/sonarqube/extensions/plugins
ADD https://github.com/checkstyle/sonar-checkstyle/releases/download/3.7/checkstyle-sonar-plugin-3.7.jar /opt/sonarqube/extensions/plugins
ADD https://github.com/SonarQubeCommunity/sonar-pmd/releases/download/2.6/sonar-pmd-plugin-2.6.jar /opt/sonarqube/extensions/plugins
ADD https://github.com/SonarQubeCommunity/sonar-findbugs/releases/download/3.4.4/sonar-findbugs-plugin-3.4.4.jar /opt/sonarqube/extensions/plugins
