## Piraya B.V. SonarQube Docker Image ##

Sometimes it is useful to run a SonarQube validation locally without first committing
your code and waiting for your CI server to show you all the violations your changes
might have introduced.

This Dockerfile uses the
default [SonarQube Docker image](https://hub.docker.com/_/sonarqube/) and extends it
with the plugins and rules-configuration used internally by [Piraya B.V.](https://wwww.piraya.nl).

Even though the embedded H2 database used by the default SonarQube Docker image is not meant
for production use, it is enough to quickly perform a local validation and saves a lot of
configuration work.

This Dockerfile was set up for internal use, but the code is shared publicly to help others
who want to accomplish the same thing. The image is also published on
[Docker Hub](https://hub.docker.com/r/pirayabv/sonar/).


### Building and publishing ###
If you want to create your own Dockerfile project based on this project, just change the names and
use the instructions below to build and/or publish the image.

* Make sure you're inside the project folder and build the Docker image giving it a name and a tag:

```
docker build -t [DOCKER_HUB_ACCOUNT_NAME]/[PROJECT_NAME]:latest -t [DOCKER_HUB_ACCOUNT_NAME]/[PROJECT_NAME]:[VERSION_NUMBER] .
```


After the file was built, you can start the docker image using:

```
docker run -p 9000:9000 -p 9092:9092 [DOCKER_HUB_ACCOUNT_NAME]/[PROJECT_NAME]
```

Once the SonarQube server is started, you can check any project you like by stepping into that projects' folder and executing:

```
mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install sonar:sonar
```


If you want to share your docker image publicly on Docker Hub, then simply login and upload your image:

```
docker login
docker push [DOCKER_HUB_ACCOUNT_NAME]/[PROJECT_NAME]
```
